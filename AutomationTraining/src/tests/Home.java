package tests;

import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.rmi.UnexpectedException;

import org.openqa.selenium.InvalidElementStateException;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import pages.HomePage;

public class Home extends TestBase {
	String pageUrl;
	String browser;
	WebDriver driver;
	HomePage homePage;
	
	@Test(dataProvider = "hardCodedBrowsers")
	public void mainPageVisualization(String browser, String version, String os, Method method)
            throws MalformedURLException, InvalidElementStateException, UnexpectedException {
		this.createDriver(browser, version, os, method.getName());
        WebDriver driver = this.getWebDriver();
        homePage = new HomePage(driver);
        
        this.annotate("Opening page");
        driver.get("https://murmuring-reef-30764.herokuapp.com/");
		
		// 2. Assert �Welcome to the Sample App� title is displayed.
		String title = "Welcome to the Sample App";
		this.annotate("Asserting title error");
		Assert.assertTrue(homePage.getWelcomeMessage().equals(title), "Title error");
		
		// 3. Assert �Sign up now� button is visible and enabled.
		this.annotate("Asserting signup button");
		Assert.assertTrue(homePage.signUpEnabled(), "Signup button error");
		
		// 4. Assert Rails Logo is present.
		this.annotate("Asserting logo");
		Assert.assertTrue(homePage.logoPresent(), "Logo error");		
	}
	
/*	@BeforeMethod
	public void setUp() throws MalformedURLException {
		pageUrl = "https://murmuring-reef-30764.herokuapp.com/";
		browser = "firefox";
		driver = tools.DriverFactory.openSauce();
		// 1. Go to the main page 
		driver.get(pageUrl);
		homePage = new HomePage(driver);
	}
	
	@AfterMethod
	public void tearDown() {
		driver.quit();
	}
*/
}
