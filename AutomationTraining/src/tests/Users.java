package tests;

import java.net.MalformedURLException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import pages.AllUserPage;
import pages.HomePage;
import pages.LoginPage;
import pages.UserPage;

public class Users {
	String homeUrl;
	String loginUrl;
	String name;
	String email;
	String password;
	String adminEmail;
	String adminPassword;
	String deleteMe;
	String browser;
	
	List<String> requiredMessages;
	List<String> countryOptions;
	WebDriver driver;
	HomePage homePage;
	LoginPage loginPage;
	UserPage userPage;
	AllUserPage allUserPage;

	@Test
	public void userFoundUserPage() {
		driver.get(homeUrl);
		driver.get(loginUrl);
		homePage.clickLogin();
		
		loginPage.inputEmail(email);
		loginPage.inputPassword(password);
		loginPage.clickLogIn();
		userPage.clickUsers();	
		Assert.assertTrue(allUserPage.userAppears(name),"User not displayed in lists error");
		
	}

	@Test
	public void adminSeesDelete() {
		driver.get(homeUrl);
		homePage.clickLogin();
		
     	loginPage.inputEmail(adminEmail);
		loginPage.inputPassword(adminPassword);
		loginPage.clickLogIn();
		userPage.clickUsers();
		Assert.assertTrue(allUserPage.deleteOptionAdmin(), "Delete option display for admin error");		
	} 
		
	@Test
	public void deleteUser() {
		//driver.get(homeUrl);
		//homePage.clickLogin();
		driver.get(loginUrl);
		
     	loginPage.inputEmail(adminEmail);
		loginPage.inputPassword(adminPassword);
		loginPage.clickLogIn();
		userPage.clickUsers();
		allUserPage.deletUser(deleteMe);
		Assert.assertFalse(allUserPage.userAppears(deleteMe), "User deletion error");
	} 		
	
	@BeforeMethod
	public void setUp() throws MalformedURLException {
		name = "test";
		email = "test@password.com";
		password = "password";
		adminEmail = "example@railstutorial.org";
		adminPassword = "foobar";
		deleteMe = "chumingo";
		browser = "firefox";
		
		homeUrl = "https://murmuring-reef-30764.herokuapp.com/";
		loginUrl = "https://murmuring-reef-30764.herokuapp.com/login";
		driver = tools.DriverFactory.openSauce();
		driver.manage().timeouts().implicitlyWait(15,TimeUnit.SECONDS);
		homePage = new HomePage(driver);
		loginPage = new LoginPage(driver);
		userPage = new UserPage(driver);
		allUserPage = new AllUserPage(driver);
	}
	
	@AfterMethod
	public void tearDown() {
		driver.close();
	}
}
