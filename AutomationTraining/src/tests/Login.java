package tests;

import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.rmi.UnexpectedException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.InvalidElementStateException;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import pages.HomePage;
import pages.LoginPage;
import pages.UserPage;

public class Login extends TestBase{
	String homeUrl;
	String email;
	String password;
	String inexistentUserEmail;
	String browser;
	
	WebDriver driver;
	HomePage homePage;
	LoginPage loginPage;
	UserPage userPage;

	
	@Test(dataProvider = "hardCodedBrowsers")
	public void requiredLoginFields(String browser, String version, String os, Method method)
            throws MalformedURLException, InvalidElementStateException, UnexpectedException {
		this.createDriver(browser, version, os, method.getName());
		WebDriver driver = this.getWebDriver();
	    driver.manage().timeouts().implicitlyWait(15,TimeUnit.SECONDS);
	    driver.get(homeUrl);
	    
	    homePage = new HomePage(driver);
		loginPage = new LoginPage(driver);
				
	    boolean itWorks = true;
		homePage.clickLogin();
		
		loginPage.inputEmail(email);
		loginPage.inputPassword("");
		loginPage.clickLogIn();
		itWorks = (itWorks && loginPage.confirmRequired());
		
		loginPage.inputEmail("");
		loginPage.inputPassword(password);
		itWorks = (itWorks && loginPage.confirmRequired());
		Assert.assertTrue(itWorks, "Login required fields error");
	}
	
	@Test(dataProvider = "hardCodedBrowsers")
	public void inexistingUserLogin(String browser, String version, String os, Method method)
            throws MalformedURLException, InvalidElementStateException, UnexpectedException {
		this.createDriver(browser, version, os, method.getName());
		WebDriver driver = this.getWebDriver();
	    driver.manage().timeouts().implicitlyWait(15,TimeUnit.SECONDS);
	    driver.get(homeUrl);
	    
	    homePage = new HomePage(driver);
		loginPage = new LoginPage(driver);
		
		homePage.clickLogin();
		
		loginPage.inputEmail(inexistentUserEmail);
		loginPage.inputPassword(password);
		loginPage.clickLogIn();
		Assert.assertTrue(loginPage.inexistentUserCredentials(), "Inexistent user login error");
	}
	
	@Test(dataProvider = "hardCodedBrowsers")
	public void userFoundUserPage(String browser, String version, String os, Method method)
            throws MalformedURLException, InvalidElementStateException, UnexpectedException {
		this.createDriver(browser, version, os, method.getName());
		WebDriver driver = this.getWebDriver();
	    driver.manage().timeouts().implicitlyWait(15,TimeUnit.SECONDS);
	    driver.get(homeUrl);
	    
	    homePage = new HomePage(driver);
		loginPage = new LoginPage(driver);
		userPage = new UserPage(driver);
		
		homePage.clickLogin();
		
		loginPage.inputEmail(email);
		loginPage.inputPassword(password);
		loginPage.clickLogIn();
		userPage.clickUsers();		
	}	
	
	@BeforeMethod
	public void setUp() throws MalformedURLException {
		email = "test@password.com";
		password = "password";
		inexistentUserEmail = "nonExistent@void.com";
		browser = "firefox";
		
		homeUrl = "https://murmuring-reef-30764.herokuapp.com/";
	}

	/*
		driver = tools.DriverFactory.openSauce();
		driver.manage().timeouts().implicitlyWait(15,TimeUnit.SECONDS);
		driver.get(homeUrl);
		
		homePage = new HomePage(driver);
		loginPage = new LoginPage(driver);
		userPage = new UserPage(driver);
	}
	
	@AfterMethod
	public void tearDown() {
		driver.close();
	}
*/
}
