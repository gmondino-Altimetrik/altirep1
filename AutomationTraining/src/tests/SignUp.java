package tests;

import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import pages.HomePage;
import pages.SignupPage;

public class SignUp {
	String homeUrl;
	String signupUrl;
	String password;
	String name;
	String diffPassword;
	String takenEmail;
	String browser;
	List<String> requiredMessages;
	List<String> countryOptions;
	WebDriver driver;
	HomePage homePage;
	SignupPage signupPage;
	
	@DataProvider
	public Object[][] CorrectEmail() {
		return new Object[][] { { new String[] { tools.RandomGenerator.rdnCorrectEmail(),
			tools.RandomGenerator.rdnCorrectEmail(), tools.RandomGenerator.rdnCorrectEmail(),
			tools.RandomGenerator.rdnCorrectEmail(), tools.RandomGenerator.rdnCorrectEmail(),
			tools.RandomGenerator.rdnCorrectEmail(), tools.RandomGenerator.rdnCorrectEmail(),
			tools.RandomGenerator.rdnCorrectEmail(), tools.RandomGenerator.rdnCorrectEmail(),
			tools.RandomGenerator.rdnCorrectEmail() 
			} } };
	}
	
	@DataProvider
	public Object[][] IncorrectEmail() {
		return new Object[][] { { new String[] {
			"Abc.example.com", // no @ character
			"A@b@c@example.com", //only one @ is allowed outside quotation marks
	        "a\"b(c)d,e:f;g<h>i[j\\k]l@example.com",// none of the special characters in this local-part are allowed outside quotation marks
			"just\"not\"right@example.com", //quoted strings must be dot separated or the only element making up the local-part
			"this is\"not\\allowed@example.com", //spaces, quotes, and backslashes may only exist when within quoted strings and preceded by a backslash)
			"this\\ still\\\"not\\allowed@example.com", //even if escaped (preceded by a backslash), spaces, quotes, and backslashes must still be contained by quotes
			"1234567890123456789012345678901234567890123456789012345678901234+x@example.com", // too long
			"john..doe@example.com", // double dot before @
			"john.doe@example..com", // double dot after @
			"\" \"@example.org", //space between the quotes
			} } };
	}
	
	@Test
	public void signUpFormRequiredFields() {
		
		//1. Go to application main page.
		driver.get(homeUrl);
		
		//2. Click on �Signup now� button.
		homePage.clickSignUp();
		
		//3. Perform the following validations on the fields:
		//Name, Email, Password and Confirmation fields are required.
		signupPage.clickCreateAccount();
		List<String> required = signupPage.getRequiredFields();
		boolean confirmRequired = equalLists(required,requiredMessages);
		if (!confirmRequired) {
			List<String> printMissing = diff(required,requiredMessages);
			System.out.println("Missing:");
			for(String s : printMissing) {
				System.out.println(s);
			}
		}
		Assert.assertTrue("Required fields error",confirmRequired);
	}
	
	@Test
	public void countrySelectOptions() {		
		driver.get(signupUrl);
		
		//Country select contains only the following values: Uruguay, Noruega, Etiopia, Surinam.
		List<String> optionsSelect = signupPage.getSelectFields();
		boolean confirmOptions = equalLists(optionsSelect,countryOptions);
		Assert.assertTrue("Country selection error",confirmOptions);
	}	
	
	@Test(dataProvider = "CorrectEmail")
	public void signUpFormFieldsValidation(String[] correctEmails) {
		driver.get(signupUrl);
		
		//Email field only accepts Strings which are in Email format.
		boolean validEmail = true;
		for(String s: correctEmails) {
			signupPage.inputName("name");
			signupPage.inputEmail(s);
			signupPage.inputPassword(password);
			signupPage.inputConfirmation(password);
			signupPage.clickCreateAccount();
			validEmail = (signupPage.correctEmail() && validEmail);
			if (!validEmail) { break;} 
			driver.get(signupUrl);
		}
		Assert.assertTrue("Correct email errors" ,validEmail);
	}
	
	@Test(dataProvider = "IncorrectEmail")
	public void incorrectEmailValidation(String[] incorrectEmails) {
		driver.get(signupUrl);
	
		//Email field only accepts Strings which are in Email format.
		boolean invalidEmail = true;
		for(String s: incorrectEmails) {
			signupPage.inputName("name");
			signupPage.inputEmail(s);
			signupPage.inputPassword("123456");
			signupPage.inputConfirmation("123456");
			signupPage.clickCreateAccount();
			invalidEmail = (signupPage.isIncorrectEmail() && invalidEmail);
			if (!invalidEmail) { break; }
			driver.get(signupUrl);
		}
		Assert.assertTrue("Incorrect email errors" ,invalidEmail);		
	}
	
	@Test
	public void mismatchPassword() {
		driver.get(signupUrl);
		
		//Password and Confirmation must match.
		signupPage.inputName(name);
		signupPage.inputEmail(tools.RandomGenerator.safeEmail());
		signupPage.inputPassword(password);
		signupPage.inputConfirmation(diffPassword);
		signupPage.clickCreateAccount();
		Assert.assertTrue("Password missmatch error", signupPage.differentPasswords());				
	}
	
	@Test
	public void matchPassword() {
		driver.get(signupUrl);		
		signupPage.inputName(name);
		signupPage.inputEmail(tools.RandomGenerator.safeEmail());
		signupPage.inputPassword(password);
		signupPage.inputConfirmation(password);
		signupPage.clickCreateAccount();
		Assert.assertTrue("Password match error", signupPage.successCreation());
	} 
	
	@Test
	public void succesfulSignup() {
		driver.get(signupUrl);		
		signupPage.inputName(name);
		signupPage.inputEmail(tools.RandomGenerator.safeEmail());
		signupPage.inputPassword(password);
		signupPage.inputConfirmation(password);
		signupPage.clickCreateAccount();
		Assert.assertTrue("Successful signup page error", signupPage.successCreationPage(name));
	}
	
	@Test
	public void duplicateSignup() {
		driver.get(signupUrl);		
		signupPage.inputName(name);
		signupPage.inputEmail(takenEmail);
		signupPage.inputPassword(password);
		signupPage.inputConfirmation(password);
		signupPage.clickCreateAccount();
		Assert.assertTrue("Duplicate signup page error", signupPage.emailIsTaken());
	}

	
	private boolean equalLists(List<String> l1, List<String> l2) {
		return (l1.containsAll(l2)&&l2.containsAll(l1)&&l2.size()==l1.size());
	}
	
	private List<String> diff(List<String> l1,List<String> l2) {
		List<String> ret = new ArrayList<String>();
		for(String s: l2) {
			if(l1.contains(s)) {
				ret.add(s);
			}
		}
		return ret;		
	}
	
	@BeforeMethod
	public void setUp() throws MalformedURLException {
		homeUrl = "https://murmuring-reef-30764.herokuapp.com/";
		signupUrl = "https://murmuring-reef-30764.herokuapp.com/signup";
		password = "password1";
		name = "name";
		diffPassword = "password2";
		takenEmail = "gmondino@Altimetrik.com";
		browser = "firefox";
		
		requiredMessages = new ArrayList<String>();
		requiredMessages.add("Name can't be blank");
		requiredMessages.add("Email can't be blank");
		requiredMessages.add("Password can't be blank");
		requiredMessages.add("Password can't be blank");
		
		countryOptions = new ArrayList<String>();
		countryOptions.add("Uruguay");
		countryOptions.add("Noruega");
		countryOptions.add("Etiopia");
		countryOptions.add("Surinam");		
		
		driver = tools.DriverFactory.openSauce();
		driver.manage().timeouts().implicitlyWait(15,TimeUnit.SECONDS);
		homePage = new HomePage(driver);
		signupPage = new SignupPage(driver);
	}
	
	@AfterMethod
	public void tearDown() {
		driver.close();
	}
}
