package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class UserPage {
	WebDriver driver;
	
	public UserPage(WebDriver driver) {
		this.driver = driver;
	}
	
	public void clickUsers() {
		driver.findElement(By.xpath("/html/body/header/div/nav/ul/li[3]/a")).click();
	}
}
