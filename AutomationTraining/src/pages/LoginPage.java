package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LoginPage {
	WebDriver driver;

	@FindBy(xpath="/html/body/div[1]/div[1]")
	WebElement singleErrorMessage;
	
	@FindBy(id="session_email")
	WebElement emailInput;
	
	@FindBy(id="session_password")
	WebElement passwordInput;
	
	@FindBy(css="input[class='btn btn-primary'][name='commit'][value='Log in']")
	WebElement loginButton;	
	
	public LoginPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	public boolean confirmRequired() {
		try {
			return singleErrorMessage.getText().contains("can't be blank");
		} catch (Exception e) {
			return false;
		}
	}
	
	public void inputEmail(String email) {
		emailInput.clear();
		emailInput.sendKeys(email);
	}
	
	public void inputPassword(String pass) {
		passwordInput.clear();
		passwordInput.sendKeys(pass);
	}
	
	public void clickLogIn() {
		loginButton.click();
	}
	
	public boolean inexistentUserCredentials() {
		try {
			return singleErrorMessage.getText().contains("Invalid email/password combination");
		} catch (Exception e) {
			return false;
		}
	}
}
