package pages;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

public class SignupPage {
    WebDriver driver;

    @FindBy(css="input[class='btn btn-primary'][value='Create my account'][type='submit']")
	WebElement createAccountButton;
    
    @FindBy(xpath="/html/body/div[1]/div/div/form/div[1]/ul")
	WebElement messageContainer;
    
    @FindBy(id="user_name")
	WebElement nameInput;
    
    @FindBy(id="user_email")
	WebElement emailInput;
    
    @FindBy(id="user_password")
	WebElement passwordInput;
    
    @FindBy(id="user_password_confirmation")
	WebElement passwordConfirmationInput;    
    
    @FindBy(xpath="/html/body/div/div[1]")
	WebElement singleMessageContainer;
    
    @FindBy(xpath="/html/body/div[1]/div[2]/aside/section/h1")
	WebElement nameDisplayer;
    
    @FindBy(xpath="/html/body/div[1]/div[2]/aside/section/h1/img")
	WebElement avatarImage;
    
    @FindBy(xpath="/html/body/div[1]/a/img")
	WebElement kittenImage;   
    
	public SignupPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	public List<String> getRequiredFields() {
		List<String> req = new ArrayList<String>();
		try {
	//		WebElement messageContainer = driver.findElement(By.xpath("/html/body/div[1]/div/div/form/div[1]/ul"));
			List<WebElement> messages = messageContainer.findElements(By.tagName("li"));
			for(WebElement m: messages) {
				if (m.getText().contains("can't be blank")) {
					req.add(m.getText());
				}
			}
		} catch (Exception e) {	}
		return req;
	}
	
	public void clickCreateAccount() {
		createAccountButton.click();
	}
	
	public List<String> getSelectFields(){
		List<String> ret = new ArrayList<String>();
		Select country = new Select(driver.findElement(By.id("user_country")));
		List<WebElement> countryOptions = country.getOptions();
		for(WebElement w: countryOptions) {
			ret.add(w.getText());
		}
		return ret;
	}
	
	public void inputName(String name) {
		nameInput.clear();
		nameInput.sendKeys(name);
	}
	
	public void inputEmail(String email) {
		emailInput.clear();
		emailInput.sendKeys(email);
	}
	
	public void inputCountry(String country) {
		new Select(driver.findElement(By.id("user_country"))).selectByVisibleText(country);
	}
	
	public void inputPassword(String password) {
		passwordInput.clear();
		passwordInput.sendKeys(password);
	}
	
	public void inputConfirmation(String confirmation) {
		passwordConfirmationInput.clear();
		passwordConfirmationInput.sendKeys(confirmation);		
	}
	
	public boolean isIncorrectEmail() {
		try {
		//	WebElement messageContainer = driver.findElement(By.xpath("/html/body/div[1]/div/div/form/div[1]/ul"));
			List<WebElement> messages = messageContainer.findElements(By.tagName("li"));
			for(WebElement m: messages) {
				if (m.getText().contains("Email is invalid")) {
					return true;
				}
			}
			return false;
		} catch (Exception e) {	
			try {
				boolean ret = createAccountButton.isDisplayed();
				return ret;
			} catch(Exception t) {
			return false;
			}
		}
	}
	
	public boolean correctEmail() {
		if (successCreation()) {
			return true;
		} else {
			try {
		//		WebElement messageContainer = driver.findElement(By.xpath("/html/body/div[1]/div/div/form/div[1]/ul"));
				List<WebElement> messages = messageContainer.findElements(By.tagName("li"));
				for(WebElement m: messages) {
					if (m.getText().contains("taken")) {
						return true;
					}
				}
				return false;
			} catch (Exception t) {	
				return false;
			}
		}
	}

	public boolean differentPasswords() {
		try {
		//	WebElement messageContainer = driver.findElement(By.xpath("/html/body/div[1]/div/div/form/div[1]/ul"));
			List<WebElement> messages = messageContainer.findElements(By.tagName("li"));
			for(WebElement m: messages) {
				if (m.getText().contains("Password confirmation doesn't match Password")) {
					return true;
				}
			}
			return false;
		} catch (Exception t) {	
			return false;
		}
	}
	
	public boolean successCreation() {
		try {
			String message = singleMessageContainer.getText();
			return message.contains("Welcome");
		} catch (Exception e) {
			return false;
		}
	}
	
	public boolean successCreationPage(String name) {
		try {
			String message = singleMessageContainer.getText();
			String userName = nameDisplayer.getText();
			boolean avatar = avatarImage.isDisplayed();
			driver.switchTo().frame(driver.findElement(By.id("imgbox")));
			boolean kitten = kittenImage.isDisplayed();
			driver.switchTo().defaultContent();
			return (message.equals("Welcome to the Sample App!") && userName.equals(name) && avatar && kitten);
		} catch (Exception e) {
			return false;
		}
	}
	
	public boolean emailIsTaken() {
		try {
	//		WebElement messageContainer = driver.findElement(By.xpath("/html/body/div[1]/div/div/form/div[1]/ul"));
			List<WebElement> messages = messageContainer.findElements(By.tagName("li"));
			for(WebElement m: messages) {
				if (m.getText().contains("Email has already been taken")) {
					return true;
				}
			}
			return false;
		} catch (Exception t) {	
			return false;
		}
	}

}
