package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class HomePage {
	WebDriver driver;
	
	@FindBy(xpath="/html/body/div/div/h1")
	WebElement welcomeMessage;
	
	@FindBy(css="a[class='btn btn-lg btn-primary'][href='/signup']")
	WebElement signupButton;
	
	@FindBy(xpath="/html/body/div/a/img")
	WebElement logo;
	
	@FindBy(xpath="/html/body/header/div/nav/ul/li[4]/a")
	WebElement dropMenu;
	
	@FindBy(xpath="/html/body/header/div/nav/ul/li[4]/ul/li[4]/a")
	WebElement logoutButton;
	
	@FindBy(css="a[href='/login']")
	WebElement loginButton;
	
	
	
	public HomePage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	public String getWelcomeMessage() {
		try {
			return welcomeMessage.getText();
		} catch (Exception e) {
			return "Not found";
		}
	}
	
	public boolean signUpEnabled() {
		try {
			return (signupButton.isDisplayed()&&signupButton.isEnabled());
		} catch (Exception e) {
			return false;
		}
	}
	
	public boolean logoPresent() {
		try {
			return logo.isDisplayed();
		} catch (Exception e) {
			return false;
		}
	}
	
	public void clickSignUp() {
		signupButton.click();
	}
	
	public void clickLogin() {
		try {
			dropMenu.click();
			logoutButton.click();
		} catch (Exception e) {	}		
		loginButton.click();
	}
	
}
