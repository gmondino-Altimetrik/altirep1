package pages;

import java.util.List;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class AllUserPage {
	WebDriver driver;
	
	public AllUserPage(WebDriver driver) {
		this.driver = driver;
	}
	
	public boolean userAppears(String name) {
		WebElement pageContainer;	
		WebElement listContainer;
		WebElement activePage;
		WebElement link;
		List<WebElement> links;
		List<WebElement> users;
		
		//Look for the user
		boolean keepLooking = true;
		while(keepLooking) {
			driver.navigate().refresh();
			pageContainer = driver.findElement(By.cssSelector("ul[class='pagination']"));
			listContainer = driver.findElement(By.cssSelector("ul[class='users']"));
			users = listContainer.findElements(By.tagName("a"));
			for(WebElement we: users) {
				if (we.getText().equals(name)) {
					return true;
				}
			}
			
			activePage = pageContainer.findElement(By.cssSelector("li[class='active']"));
			link = activePage.findElement(By.tagName("a"));
			String number = link.getText();
			
			if (number.equals("6")) {	//LastPage
				keepLooking = false;
			} else {
				int nextNumber = Integer.parseInt(number) + 1;
				links = pageContainer.findElements(By.tagName("a"));
				for(WebElement l: links) {
					if (l.getText().contains(Integer.toString(nextNumber))) {
						l.click();
					}
				}
			}			
		}			
		return false;
	}
	
	public boolean deleteOptionAdmin() {
    	WebElement listContainer = driver.findElement(By.cssSelector("ul[class='users']"));
		List<WebElement> users = listContainer.findElements(By.cssSelector("img"));
		List<WebElement> deletes = listContainer.findElements(By.cssSelector("a[data-confirm='You sure?'][data-method='delete']"));
		return users.size()==deletes.size();
	}

	public void deletUser(String name) {
		WebElement pageContainer;
		WebElement listContainer;
		List<WebElement> users;
		WebElement activePage;
		WebElement link;
		List<WebElement> links;
		
		boolean keepLooking = true;
		while(keepLooking) {
			driver.navigate().refresh();
			pageContainer = driver.findElement(By.cssSelector("ul[class='pagination']"));
			listContainer = driver.findElement(By.cssSelector("ul[class='users']"));
			users = listContainer.findElements(By.tagName("li"));
			
			for(WebElement we: users) {
				if (we.findElement(By.xpath("a[1]")).getText().equals(name)) {
					we.findElement(By.cssSelector("a[data-method='delete']")).click();
					Alert alert = driver.switchTo().alert();
				    alert.accept();
					keepLooking = false;
					break;
				}
			}			
			activePage = pageContainer.findElement(By.cssSelector("li[class='active']"));
			link = activePage.findElement(By.tagName("a"));
			String number = link.getText();
			
			if (number.equals("6")) {	//LastPage
				keepLooking = false;
			} else {
				int nextNumber = Integer.parseInt(number) + 1;
				links = pageContainer.findElements(By.tagName("a"));
				for(WebElement l: links) {
					if (l.getText().contains(Integer.toString(nextNumber))) {
						l.click();
					}
				}
			}			
		}			
	} 
}
