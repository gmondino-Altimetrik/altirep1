package tools;

import java.util.Random;

public class RandomGenerator {

	private static String rndDomain() {
		String chars = "AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz1234567890-.";
		StringBuilder domain = new StringBuilder();
		char lastChar = '-';
		char nextChar;
		boolean fst,snd,trd,fth,ffh;
		Random rnd = new Random();
		
		while (domain.length() < 4) {
			do {
			int index = (int) (rnd.nextFloat() * chars.length());
			nextChar = chars.charAt(index);
			
			//Most common domain restrictions
			fst = (domain.length() == 0) && (nextChar == '-'); // '-' cant be the first character
			snd = (domain.length() == 4) && (nextChar == '-'); // '-' cant be the last character
			trd = (nextChar == '.') && (lastChar == '.'); // not allowed two consecutive points
			fth = (domain.length() == 4) && (nextChar == '.'); // '.' cant be the last character
			ffh = (domain.length() == 0) && (nextChar == '.'); // '.' cant be the first character
			} while(fst||snd||trd||fth||ffh);
			
			lastChar = nextChar;
			domain.append(nextChar);
		}		
		return domain.toString();    
	}
	
	private static String rndString() {
		String chars = "AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz1234567890!#$%&'*+-/=?^_`{|}~.";
		StringBuilder stvar = new StringBuilder();
		char lastChar = '-';
		char nextChar;
		boolean fst;
		Random rnd = new Random();
		
		while (stvar.length() < 12) {
			do {
			int index = (int) (rnd.nextFloat() * chars.length());
			nextChar = chars.charAt(index);
			fst = (nextChar == '.') && (lastChar == '.'); // not allowed two consecutive points
			} while(fst);
			lastChar = nextChar;
			stvar.append(nextChar);
		}		
		return stvar.toString();    
	}
	
	public static String rdnCorrectEmail() {
		return rndString() + "@" + rndDomain() + "." + rndDomain();
	}
	
	public static String safeEmail() {
		Random rnd = new Random();
		int index = (int) (rnd.nextFloat() * 1000000);
		return "safeEmailTest" + index + "@test.com";
	}
}
