package tools;

import java.net.MalformedURLException;
import java.net.URL;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

public class DriverFactory {
	static String driverLocation = "C:\\Users\\gmondino\\Desktop\\Historial\\1 - Training\\DriversSlenium\\";
	public static final String USERNAME = "gmondino";
	public static final String ACCESS_KEY = "d84d86b4-6822-4489-b1bd-e27448ae8a72";
	public static final String URL = "https://" + USERNAME + ":" + ACCESS_KEY + "@ondemand.saucelabs.com:443/wd/hub";
	
	public static WebDriver open(String browserType) {
		if (browserType.equalsIgnoreCase("firefox")) {
			System.setProperty("webdriver.gecko.driver", driverLocation + "geckodriver.exe");
			return new FirefoxDriver();
		} else if (browserType.equalsIgnoreCase("IE")) {
			System.setProperty("webdriver.ie.driver", driverLocation + "IEDriverServer.exe");
			return new InternetExplorerDriver();
		} else { 
			System.setProperty("webdriver.chrome.driver", driverLocation + "chromedriver.exe");
			return new ChromeDriver();
		}
	}
	
	public static WebDriver openSauce() throws MalformedURLException {
		DesiredCapabilities caps = DesiredCapabilities.firefox();
	    caps.setCapability("platform", "Windows 10");
	    caps.setCapability("version", "59");
	    caps.setCapability("name", "Sauce Test");
	    
	    return new RemoteWebDriver(new URL(URL), caps);
	}
}
