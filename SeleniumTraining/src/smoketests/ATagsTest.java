package smoketests;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class ATagsTest {

	WebDriver driver; 
	
	@Test
	public void loginElementsPresentTest() {
		System.out.println("Running test");
		boolean createAccount = false;
		
		// We want to test the presence of A >> hyperlinks	
		List <WebElement> Elements = driver.findElements(By.tagName("a"));
		int numberOfAElements = Elements.size();
		System.out.println("There are " + numberOfAElements + "a tags on the page");
		
		for (WebElement elem : Elements) {
			System.out.println(elem.getText());
			if (elem.getText().equals("CREATE ACCOUNT")) {
				createAccount = true;
				break;
			}
		}
		
		Assert.assertTrue(createAccount);
	}
	
	@BeforeMethod
	public void setup() {
		System.out.println("Starting test");
		String webURL = "http://sdettraining.com/trguitransactions/AccountManagement.aspx";
		
		driver = utilities.DriverFactory.open("chrome");
		driver.get(webURL);
	}
	
	@AfterMethod
	public void tearDown() {
		System.out.println("Closing test");
		driver.close();
	}
}
