package demos;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class Login {

	public static void main(String[] args) {
		// 1. Define the web driver
		// Once the property is set the driver object can be instantiated
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\gmondino\\Desktop\\Historial\\1 - Training\\DriversSlenium\\chromedriver.exe");
		WebDriver driverInstance = new ChromeDriver();
		
		// 2. Open the web browser and navigate to page
		// http://sdettraining.com/trguitransactions/AccountManagement.aspx
		driverInstance.get("http://sdettraining.com/trguitransactions/AccountManagement.aspx");
	
		//Find elements:
			//Locate the element, Determine the action, Pass any parameters
		
		// 3. Enter email address
		driverInstance.findElement(By.name("ctl00$MainContent$txtUserName")).sendKeys("tim@testemail.com");
		
		// 4. Enter password
		driverInstance.findElement(By.id("MainContent_txtPassword")).sendKeys("trpass");
		
		// 5. Click login
		driverInstance.findElement(By.name("ctl00$MainContent$btnLogin")).click();
		
		// 6. Get confirmation
		String message = driverInstance.findElement(By.id("conf_message")).getText();
		System.out.println("Result: " + message);
		
		String pageTitle = driverInstance.getTitle();
		System.out.println("Page title: " + pageTitle);
		
		// 7. Close the browser
		driverInstance.close();

	}

}
