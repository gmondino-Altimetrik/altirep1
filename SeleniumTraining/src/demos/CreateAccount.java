package demos;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

public class CreateAccount {

	public static void main(String[] args) {
		// 1. Create WebDriver
		System.setProperty("webdriver.gecko.driver","C:\\Users\\gmondino\\Desktop\\Historial\\1 - Training\\DriversSlenium\\geckodriver.exe");
		WebDriver fDriver = new FirefoxDriver();
		
		// 2. Open the browser to account management page >> Click on create account
		fDriver.get("http://sdettraining.com/trguitransactions/AccountManagement.aspx");
		fDriver.findElement(By.linkText("Create Account")).click();
		
		// 3. Fill the form
			
			// How to locate elements 
			fDriver.findElement(By.name("ctl00$MainContent$txtFirstName")).sendKeys("Marry Smith"); //name
			fDriver.findElement(By.id("MainContent_txtEmail")).sendKeys("ms@testmail.com"); //email
			
			// This is a relative XPath, 
			fDriver.findElement(By.xpath("//*[@id='MainContent_txtHomePhone']")).sendKeys("989982003"); //phone
			// This is an absolute XPath
			// fDriver.findElement(By.xpath("html/body/form/div[3]/div[2]/div/div[2]/div[3]/div[2]/input")).sendKeys("123");
			
			// The CSS selector can have multiple css attributes for example: cssSelector("input[type='password'][id='blabla'][...]"
			fDriver.findElement(By.cssSelector("input[type='password']")).sendKeys("mspass"); //password
			
			// XPath and CSS selector are a good backup when the name or id aren't available
			
			fDriver.findElement(By.name("ctl00$MainContent$txtVerifyPassword")).sendKeys("mspass"); //verify password
			
			// How to interact with HTML elements
			fDriver.findElement(By.id("MainContent_Female")).click(); //gender radio button
			fDriver.findElement(By.cssSelector("input[name='ctl00$MainContent$Gender'][value='Male']")).click(); //gender radio button
			
			new Select(fDriver.findElement(By.id("MainContent_menuCountry"))).selectByVisibleText("Canada"); //dropdown picklist
			
			fDriver.findElement(By.name("ctl00$MainContent$checkWeeklyEmail")).click();
			fDriver.findElement(By.id("MainContent_btnSubmit")).click();
			
		// 4. Get confirmation
		String conf = fDriver.findElement(By.id("MainContent_lblTransactionResult")).getText();
		System.out.println("Confirmation: " + conf);
		
		// 5. Close the browser
		fDriver.close();		
		
	}

}
