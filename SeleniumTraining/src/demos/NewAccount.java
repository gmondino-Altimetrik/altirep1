package demos;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

public class NewAccount {

	public static void main(String[] args) {
		String name = "Marry Smith";
		String email = "ms@testmail.com";
		String password = "mspass";
		String country = "Canada";
		String phoneNumber = "989982003";
		String browserType = "firefox";		
		String gender = "female";
		Boolean weeklyEmail = true;
		Boolean monthlyEmail = false;
		Boolean occassionalEmail = false;
		
		// 1. Open the browser to account management page >> Click on create account
		// Locate elements and define web elements
		
		WebDriver driver;
		driver = utilities.DriverFactory.open(browserType);
		driver.get("http://sdettraining.com/trguitransactions/AccountManagement.aspx");
		driver.findElement(By.xpath("html/body/form/div[3]/div[2]/div/div[2]/a")).click();
		
		WebElement nameElement = driver.findElement(By.name("ctl00$MainContent$txtFirstName")); //name
		WebElement emailElement = driver.findElement(By.id("MainContent_txtEmail")); //email
		WebElement phoneElement = driver.findElement(By.xpath("//*[@id='MainContent_txtHomePhone']")); // phone
		WebElement passwordElement = driver.findElement(By.cssSelector("input[type='password']")); //password
		WebElement passwordVerifyElement = driver.findElement(By.name("ctl00$MainContent$txtVerifyPassword")); //verify password
		WebElement countryElement = driver.findElement(By.id("MainContent_menuCountry"));
		WebElement femaleRadio = driver.findElement(By.id("MainContent_Female"));
		WebElement maleRadio = driver.findElement(By.cssSelector("input[name='ctl00$MainContent$Gender'][value='Male']")); //gender radio button
		WebElement weeklyCheckbox = driver.findElement(By.name("ctl00$MainContent$checkWeeklyEmail"));
		WebElement monthlyCheckbox = driver.findElement(By.name("ctl00$MainContent$checkMonthlyEmail"));
		WebElement occasionalCheckbox = driver.findElement(By.name("ctl00$MainContent$checkUpdates")); 
		WebElement submitButton = driver.findElement(By.id("MainContent_btnSubmit"));
		
		// 2. Fill the form
						 
		nameElement.sendKeys(name);
		emailElement.sendKeys(email);
		phoneElement.sendKeys(phoneNumber);
		passwordElement.sendKeys(password);	
		passwordVerifyElement.sendKeys(password);	
		new Select(countryElement).selectByVisibleText(country); //dropdown picklist
					
		// How to interact with HTML elements
		// gender radio button
		if (gender.equalsIgnoreCase("male")) {
			femaleRadio.click();
		} else {
			maleRadio.click();
		}
		
		// checkbox
		//unselect the options
		if (weeklyCheckbox.isSelected())	 {weeklyCheckbox.click();}
		if (monthlyCheckbox.isSelected())	 {monthlyCheckbox.click();}
		if (occasionalCheckbox.isSelected()) {occasionalCheckbox.click();}
		// check the option
		if (weeklyEmail) {weeklyCheckbox.click();
		} else if (monthlyEmail) {monthlyCheckbox.click();
		} else {occasionalCheckbox.click();}
		
		submitButton.click();
									
		// 3. Get confirmation and close the browser
		String conf = driver.findElement(By.id("MainContent_lblTransactionResult")).getText();
		String expected = "Customer information added successfully";
		if (conf.contains(expected)) {
			System.out.println("Confirmation: " + conf);
		} else {
			System.out.println("Test failed");
		}
		driver.close();
			
		// For the phone a relative xpath is being used but also absolute xpath exists
		// For example: 
		// 	WebElement phoneNumber = driver.findElement(By.xpath("html/body/form/div[3]/div[2]/div/div[2]/div[3]/div[2]/input"));
		// The CSS selector can have multiple css attributes 
		// For example:
		//		cssSelector("input[type='password'][id='blabla'][...]"
		// XPath and CSS selector are a good backup when the name or id aren't available
		
	}

}
