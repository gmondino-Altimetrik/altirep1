package demos;

import java.util.List;

public class DataReaders {

	public static void main(String[] args) {
		readXLS();
	}
	
	public static void CSV() {
		String fileName = "C:\\Users\\gmondino\\Desktop\\Historial\\1 - Training\\UserAccounts.csv";
		List <String []> records = utilities.CSV.get(fileName);
		
		for (String [] record: records) {
			for(String field: record) {
				System.out.println(field);
			}
		}		
	}
	
	public static void readXLS() {
		String fileName = "C:\\Users\\gmondino\\Desktop\\Historial\\1 - Training\\UserLogin.xls";
		String [] [] data = utilities.Excel.get(fileName);
		for (String [] record: data) {
			System.out.println("\nNew Record");
			System.out.println(record[0]);
			System.out.println(record[1]);
			System.out.println(record[2]);
		}
	}

}
