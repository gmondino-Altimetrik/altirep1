package tests;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class LoginDDT {
	WebDriver driver;
	
	@Test(dataProvider = "getData")
	public void loginTest(String name, String email, String password) {
		System.out.println("Running the test");
		// Interact with login elements
		driver.findElement(By.name("ctl00$MainContent$txtUserName")).sendKeys(email);
		driver.findElement(By.id("MainContent_txtPassword")).sendKeys(password);
		driver.findElement(By.name("ctl00$MainContent$btnLogin")).click();
			
		// Confirmation
		String message = driver.findElement(By.id("conf_message")).getText();
		System.out.println("Confirmation: " + message);
	}
	
	@BeforeMethod
	public void setUp() {
		System.out.println("Starting the test");
		// Define the web driver
		driver = utilities.DriverFactory.open("firefox");
		driver.get("http://sdettraining.com/trguitransactions/AccountManagement.aspx");
	}
	
	@AfterMethod
	public void tearDown() {
		System.out.println("Closing the test");
		driver.close();
	}
	
	@DataProvider 
	public String[][] getData() {
		return utilities.Excel.get("C:\\Users\\gmondino\\Desktop\\Historial\\1 - Training\\UserLogin.xls");
	}
}
