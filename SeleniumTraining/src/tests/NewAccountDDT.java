package tests;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

@RunWith(value = Parameterized.class)
public class NewAccountDDT {
	String name, email, phone, gender, password, country;
	boolean weeklyEmail, monthlyEmail, occasionalEmail;
	WebElement nameElement, emailElement, phoneElement, passwordElement, passwordVerifyElement, countryElement, femaleRadio, maleRadio;
	WebElement weeklyCheckbox, monthlyCheckbox, occasionalCheckbox, submitButton;
	WebDriver driver;

	@Test
	public void newAccountTest() {
		System.out.println("New record: "+ name + " " + email + " "+ phone+ " " + gender + " " + password + " " + country + " " + weeklyEmail + " " + monthlyEmail + " " + occasionalEmail);
	
		// Define web elements
		defineWebElements();
		
		// Fill the form		 
		nameElement.sendKeys(name);
		emailElement.sendKeys(email);
		phoneElement.sendKeys(phone);
		passwordElement.sendKeys(password);	
		passwordVerifyElement.sendKeys(password);	
		new Select(countryElement).selectByVisibleText(country);
				
		// How to interact with HTML elements
		// gender radio button
		if (gender.equalsIgnoreCase("male")) {
			femaleRadio.click();
		} else {
			maleRadio.click();
		}
		
		// checkbox
		//unselect the options
		if (weeklyCheckbox.isSelected())	 {weeklyCheckbox.click();}
		if (monthlyCheckbox.isSelected())	 {monthlyCheckbox.click();}
		if (occasionalCheckbox.isSelected()) {occasionalCheckbox.click();}
		// check the option
		if (weeklyEmail) {weeklyCheckbox.click();
		} else if (monthlyEmail) {monthlyCheckbox.click();
		} else {occasionalCheckbox.click();}
		
		submitButton.click();							
		
	}
	
	@Before
	public void setup() {
		driver = utilities.DriverFactory.open("chrome");
		driver.get("http://sdettraining.com/trguitransactions/AccountManagement.aspx");
		driver.findElement(By.xpath("html/body/form/div[3]/div[2]/div/div[2]/a")).click();
	}
	
	@After
	public void tearDown() {
		driver.quit();
	}
	
	public void defineWebElements() {
		nameElement = driver.findElement(By.name("ctl00$MainContent$txtFirstName")); //name
		emailElement = driver.findElement(By.id("MainContent_txtEmail")); //email
		phoneElement = driver.findElement(By.xpath("//*[@id='MainContent_txtHomePhone']")); // phone
		passwordElement = driver.findElement(By.cssSelector("input[type='password']")); //password
		passwordVerifyElement = driver.findElement(By.name("ctl00$MainContent$txtVerifyPassword")); //verify password
		countryElement = driver.findElement(By.id("MainContent_menuCountry"));
		femaleRadio = driver.findElement(By.id("MainContent_Female"));
		maleRadio = driver.findElement(By.cssSelector("input[name='ctl00$MainContent$Gender'][value='Male']")); //gender radio button
		weeklyCheckbox = driver.findElement(By.name("ctl00$MainContent$checkWeeklyEmail"));
		monthlyCheckbox = driver.findElement(By.name("ctl00$MainContent$checkMonthlyEmail"));
		occasionalCheckbox = driver.findElement(By.name("ctl00$MainContent$checkUpdates")); 
		submitButton = driver.findElement(By.id("MainContent_btnSubmit"));
	}
	// This annotated method is designed to pass parameters into the class via constructor
	@Parameters
	public static List<String []> getData() {
		return utilities.CSV.get("C:\\Users\\gmondino\\Desktop\\Historial\\1 - Training\\UserAccounts.csv");
	}
	
	// Constructor that passes parameters to the test method
	public NewAccountDDT(String name, String email, String phone, String gender, String password, String country,
				String weeklyEmail, String monthlyEmail, String occasionalEmail) {
		this.name = name;
		this.email = email;
		this.phone = phone; 
		this.gender = gender;
		this.password = password;
		this.country = country;
		
		if (weeklyEmail.equals("TRUE")) { this.weeklyEmail = true; }
		else { this.weeklyEmail = false; } 
		
		if (monthlyEmail.equals("TRUE")) { this.monthlyEmail = true; }
		else { this.monthlyEmail = false; } 
		
		if (occasionalEmail.equals("TRUE")) { this.occasionalEmail = true; }
		else { this.occasionalEmail = false; } 
	}
}
