package tests;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class Expedia {
	WebDriver driver;
	String browserType = "chrome";
	String city = "New York, NY";
	String checkIn = "04/20/2018";
	String checkOut = "05/01/2018";
	String numOfGuests = "2";
	String searchResult = "4";
	
	@Test
	public void hotelReservation() {
		// 1. Search
		driver.findElement(By.id("tab-hotel-tab-hp")).click();
		driver.findElement(By.id("hotel-destination-hp-hotel")).sendKeys(city);
		driver.findElement(By.id("hotel-checkin-hp-hotel")).clear();
		driver.findElement(By.id("hotel-checkin-hp-hotel")).sendKeys(checkIn);
		driver.findElement(By.id("hotel-checkout-hp-hotel")).clear();
		driver.findElement(By.id("hotel-checkout-hp-hotel")).sendKeys(checkOut);
		driver.findElement(By.xpath("//*[@id=\"gcw-hotel-form-hp-hotel\"]/div[4]/div[3]/div/ul/li/button")).click();
		driver.findElement(By.xpath("//*[@id=\"gcw-hotel-form-hp-hotel\"]/div[4]/div[3]/div/ul/li/div/div/div/div[2]/div[4]/button")).click();
		driver.findElement(By.xpath("//*[@id=\"gcw-hotel-form-hp-hotel\"]/div[9]/label/button")).click();
		
		// Print the name of the city (confirmation)
		String actualCity = driver.findElement(By.xpath("//*[@id=\"hotelResultTitle\"]/h1")).getText();
		System.out.println("City: " + actualCity);
		
		
		// 2. Modify the search result page, give criteria
		driver.findElement(By.cssSelector("input[name='star'][id='star4'][type='checkbox']")).click();
		
		// 3. Analyze the results and make our selection
		
		boolean doIt = true;
		while (doIt) { 
			try { 
			//	WebElement element = driver.findElement(By.xpath("//*[@id=\"resultsContainer\"]/section/article[" + searchResult + "]/div[2]/div/a"));
			//	Actions actions = new Actions(driver);
			//	actions.moveToElement(element).click().perform();
				// Previous acctions are made becouse sometimes the selected element (like the below link) are not clickable 
				//due to location, it says is not clickable on the error log
				driver.findElement(By.xpath("//*[@id=\"resultsContainer\"]/section/article[" + searchResult + "]/div[2]/div/a")).click();
				doIt = false;
			} catch (Exception e) { 
				if (e.getMessage().contains("element is not attached")) { 
					doIt = true; 
				} 
			}
		} 
	 
	// Switch the window to the pop up
		ArrayList<String> windows = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(windows.get(1));
		
		// Print hotel name and star raiting (confirmation)
		String hotelName = driver.findElement(By.id("hotel-name")).getText();
		System.out.println("Hotel name: " + hotelName);
		
		// 4. Book reservation
		driver.findElement(By.xpath("//*[@id=\"rooms-and-rates\"]/div/article/table/tbody[1]/tr/td[4]/div/div[1]/button")).click();
		
		String hotelPrice = "";
		while (hotelPrice.equals("")) {
			hotelPrice = driver.findElement(By.xpath("//*[@id=\"payment-choice-modal\"]/div/div[1]/div/div/div[1]/div[1]/div[1]/span")).getText();
		}
		System.out.println("Hotel price: " + hotelPrice);
		
		// 5. Fill our contact / billing
		
		// 6. Get confirmation		
		String pageTitle = driver.getTitle();
		Assert.assertTrue(pageTitle.contains("Hotel") || pageTitle.contains("hotel"));
	}	
	
	@BeforeMethod
	public void setUp() {
		driver = utilities.DriverFactory.open(browserType);
		driver.manage().timeouts().implicitlyWait(20,TimeUnit.SECONDS);
		driver.get("https://www.expedia.com/");
		
	}
	
	@AfterMethod
	public void tearDown() {
		driver.quit();
	}
}
