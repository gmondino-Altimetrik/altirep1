package tests;

import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;

import pages.DashboardPage;
import pages.LoginPage;

public class Login {
	
	@Test
	public void loginTestPOM() {
	// 1. Initialize driver
	WebDriver driver = utilities.DriverFactory.open("firefox");
	driver.get("http://sdettraining.com/trguitransactions/AccountManagement.aspx");
	
	// 2. Enter login information (Login page)
	LoginPage loginPage = new LoginPage(driver);
	loginPage.setUserName("tim@testmail.com");
	loginPage.setPassword("trpass");
	loginPage.clickSubmit();
	
	// 3. Get Confirmation (Dashboard Page)
	DashboardPage dashboardPage = new DashboardPage(driver);
	String conf = dashboardPage.confirmationMessage();
	
	// 4. Assertions
	Assert.assertTrue(conf.contains("Logged"));
	

	// 4. Close the driver
	driver.quit();
	}
}
