package utilities;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

public class DriverFactory {
	
	// This method return a WebDriverMethod
	public static WebDriver open(String browserType) {
		if (browserType.equalsIgnoreCase("firefox")) {
			System.setProperty("webdriver.gecko.driver","C:\\Users\\gmondino\\Desktop\\Historial\\1 - Training\\DriversSlenium\\geckodriver.exe");
			return new FirefoxDriver();
		}
		else if (browserType.equalsIgnoreCase("IE")) {
			System.setProperty("webdriver.ie.driver", "C:\\Users\\gmondino\\Desktop\\Historial\\1 - Training\\DriversSlenium\\IEDriverServer.exe");
			return new InternetExplorerDriver();
		}
		else{ // Just do chrome if other<s
			System.setProperty("webdriver.chrome.driver", "C:\\Users\\gmondino\\Desktop\\Historial\\1 - Training\\DriversSlenium\\chromedriver.exe");
			return new ChromeDriver();
		}
	}

}
